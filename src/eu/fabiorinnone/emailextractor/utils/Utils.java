/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.utils;

import org.jetbrains.annotations.Contract;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by fabior on 22/05/16.
 */
public class Utils {

    public static String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width-1) + ".";
        else
            return s;
    }

    public static String removeFragmentFromUrl(String urlString) throws URISyntaxException {
        URI uri = new URI(urlString);
        String fragment = uri.getFragment();
        if (fragment != null) {
            urlString = urlString.replace("#" + fragment, "");
        }
        return urlString;
    }

    public static String getBaseUrl(String urlString) throws URISyntaxException {
        URI uri = new URI(urlString);
        String scheme = uri.getScheme();
        String auth = uri.getAuthority();
        urlString = scheme + "://" + auth;
        return urlString;
    }

    @Contract(pure = true)
    public static boolean isVerboseMode(int mode) {
        if (mode == Common.VERBOSE_MODE || mode == Common.DEBUG_MODE)
            return true;
        else
            return false;
    }

    @Contract(pure = true)
    public static boolean isDebugMode(int mode) {
        if (mode == Common.DEBUG_MODE)
            return true;
        else
            return false;
    }

    public static String getDateTimeFormatted(DateTime dateTime) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(Common.DATE_TIME_PATTERN);
        String dateTimeFormatted = dateTime.toString(fmt);
        return dateTimeFormatted;
    }

    public static void printHeapMemoryInfo() {
        long totalMemory = Runtime.getRuntime().totalMemory();
        long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.print("DEBUG: Java current heap space is " + totalMemory + "/" + maxMemory);
        BigDecimal totalMemoryMb =
                new BigDecimal(totalMemory / Math.pow(1024, 2)).setScale(2, BigDecimal.ROUND_CEILING);
        BigDecimal maxMemoryMb =
                new BigDecimal(maxMemory / Math.pow(1024, 2)).setScale(2, BigDecimal.ROUND_CEILING);
        System.out.println(" (" + totalMemoryMb + " MB/" + maxMemoryMb + " MB)");
    }

    public static String escapeUrl(String url) {
        return url.replaceAll("'", "\\\\'");
    }
}
