package eu.fabiorinnone.emailextractor.utils;

import eu.fabiorinnone.emailextractor.database.OrientDBAdapter;
import org.joda.time.DateTime;

import java.util.TimerTask;

/**
 * Created by fabior on 08/07/16.
 */
public class PrintMessages extends TimerTask {

    private OrientDBAdapter dbAdapter;
    private int mode;

    private int hoursElapsed;
    private int daysElapsed;
    private int weeksElapsed;

    public PrintMessages(OrientDBAdapter dbAdapter, int mode) {
        this.dbAdapter = dbAdapter;
        this.mode = mode;

        hoursElapsed = 0;
        daysElapsed = 0;
        weeksElapsed = 0;
    }

    @Override
    public void run() {
        hoursElapsed++;

        if (hoursElapsed > 0 && hoursElapsed % 24 == 0)
            daysElapsed++;
        if (daysElapsed > 0 && daysElapsed % 7 == 0)
            weeksElapsed++;
        String timeElapsed = "Time elapsed: ";

        String weeks = String.valueOf(weeksElapsed);
        String days = "";
        String hours = "";
        if (weeksElapsed > 0)
            days = String.valueOf(daysElapsed - (weeksElapsed * 7));
        else
            days = String.valueOf(daysElapsed);

        if (daysElapsed > 0)
            hours = String.valueOf(hoursElapsed - (daysElapsed * 24));
        else
            hours = String.valueOf(hoursElapsed);

        timeElapsed += weeks + " weeks " + days + " days " + hours + " hours";

        DateTime now = DateTime.now();
        System.out.println("INFO: " + Utils.getDateTimeFormatted(now) +
                ": URLs founded: " + dbAdapter.getUrlsCount() + "." +
                " Addresses founded: " + dbAdapter.getEmailCount() + ". "  +timeElapsed);

        if (Utils.isDebugMode(mode))
            Utils.printHeapMemoryInfo();
    }
}
