/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.utils;

/**
 * Created by fabior on 07/05/16.
 */
public class Common {

    public static int INFO_MODE = 0;
    public static int VERBOSE_MODE = 1;
    public static int DEBUG_MODE = 2;

    public static String URLS_CLASS_NAME = "urls";
    public static String URLS_PROP_URL = "url";
    public static String URLS_PROP_SEED = "seed";
    public static String URLS_PROP_DEPTH = "depth";
    public static String URLS_PROP_VISITED = "visited";
    public static String URLS_PROP_DATE_TIME = "date_time";

    public static String EMAILS_CLASS_NAME = "emails";
    public static String EMAILS_PROP_EMAIL = "email";
    public static String EMAILS_PROP_URL = "url";
    public static String EMAILS_PROP_KEY_1 = "key_1";
    public static String EMAILS_PROP_KEY_2 = "key_2";
    public static String EMAILS_PROP_DATE_TIME = "date_time";

    public static int MAX_SIZE = (int) Math.pow(2, 18);
    //public static int MIN_SIZE = (int) Math.pow(2, 16);
    @SuppressWarnings("unused")
    public static int MAX_RETRIES = 5;

    public static int URLS_FOR_THREAD = 25;
    public static int THREAD_NUM = 4;
    public static int START_QUEUE_SIZE = 100;

    public static String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static int INTERVAL = 60000 * 60;

    public static String[] ignoredSchemas = {"about", "acct", "crid", "data", "file", "info", "ldap", "mailto",
            "sip", "sips", "tag", "urn", "view-source", "ws", "wss", "ftp"};
}
