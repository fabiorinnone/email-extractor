/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.utils;

import org.apache.commons.collections4.list.TreeList;

/**
 * 
 * @author Fabio Rinnone
 *
 * @param <T>
 */
public class Pair<T> {
	TreeList<T> list = new TreeList<T>();
	
	public Pair(T first, T second) {
		list.add(first);
		list.add(second);
	}
	
	public T getFirst() {
		return list.get(0);
	}
	
	public T getSecond() {
		return list.get(1);
	}
	
	public void setFirst(T element) {
		list.set(0, element);
	}
	
	public void setSecond(T element) {
		list.set(1, element);
	}
}