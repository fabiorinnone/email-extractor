/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.writer;

import eu.fabiorinnone.emailextractor.utils.Pair;

import java.io.*;

/**
 * Created by fabior on 22/05/16.
 */
public class Writer {

    private String url;
    private String emailAddress;
    private Pair<String> keywords;

    public Writer(String url, String emailAddress, Pair<String> keywords) {
        this.url = url;
        this.emailAddress = emailAddress;
        this.keywords = keywords;
    }

    public void write() {
        try {
            File file = new File("out.txt");
            if (!file.exists())
                file.createNewFile();
            //else {
                //file.delete();
            //}

            BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsoluteFile(), true));

            String keywordFirst = keywords.getFirst();
            String keywordSecond = keywords.getSecond();

            String line = "{" + url + ";" + emailAddress + ";[" +keywordFirst + ";" + keywordSecond + "]}";
            StringBuffer sb = new StringBuffer(line);

            synchronized(this) {
                writer.write(sb.toString() + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
