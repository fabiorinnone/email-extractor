/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.fetcher;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import eu.fabiorinnone.emailextractor.url.UrlCanonicalizer;
import eu.fabiorinnone.emailextractor.utils.Utils;
import org.apache.commons.io.IOUtils;
import org.apache.http.*;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

/**
 * Created by fabior on 01/06/16.
 */
public class PageFetcher {

    private String url;
    private int mode;
    private int tNum;

    private PoolingHttpClientConnectionManager connectionManager;
    private CloseableHttpClient httpClient;

    @SuppressWarnings("deprecation")
    public PageFetcher(String url, int mode, int tNum) {
        this.url = url;
        this.mode = mode;
        this.tNum = tNum;

        RequestConfig requestConfig = RequestConfig.custom()
                .setExpectContinueEnabled(false)
                .setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY)
                .setRedirectsEnabled(false)
                .setConnectTimeout(10000)
                .setSocketTimeout(10000)
                .build();

        RegistryBuilder<ConnectionSocketFactory> connRegistryBuilder = RegistryBuilder.create();
        connRegistryBuilder.register("http", PlainConnectionSocketFactory.INSTANCE);
        try {
            SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustStrategy() {
                        @Override
                        public boolean isTrusted(final X509Certificate[] chain, String authType) {
                            return true;
                        }
                    }).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslContext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            connRegistryBuilder.register("https", sslsf);
        } catch (Exception e) {
            System.err.println("ERROR: Thread " + tNum + ": " + e.getMessage());
        }

        Registry<ConnectionSocketFactory> connRegistry = connRegistryBuilder.build();
        connectionManager = new PoolingHttpClientConnectionManager(connRegistry);
        connectionManager.setMaxTotal(10);
        connectionManager.setDefaultMaxPerRoute(10);

        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
        clientBuilder.setDefaultRequestConfig(requestConfig);
        clientBuilder.setConnectionManager(connectionManager);

        httpClient = clientBuilder.build();
    }

    public String fetchPage() throws IOException {
        if (Utils.isVerboseMode(mode))
            System.out.println("INFO: Thread " + tNum + ": Fetching page at url " + url);

        HttpGet get = null;
        HttpEntity entity = null;

        String contentType = null;
        String content = null;

        try {
            get = new HttpGet(url);

            HttpResponse response = httpClient.execute(get);
            entity = response.getEntity();

            contentType = null;
            Header type = entity.getContentType();
            if (type != null) {
                contentType = type.getValue();
            }

            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode == HttpStatus.SC_OK) { // is 200, everything looks ok
                String uri = get.getURI().toString();
                if (!uri.equals(url)) {
                    if (!UrlCanonicalizer.getCanonicalURL(uri).equals(url)) {
                        url = uri;
                    }
                }
            }

        } finally { // occurs also with thrown exceptions
            if (entity == null && get != null) {
                get.abort();
            }
        }

        if (!hasBinaryContent(contentType) && !hasPlainTextContent(contentType)) {
            InputStream inputStream = entity.getContent();

            //if (Utils.isDebugMode(mode))
                //Utils.printHeapMemoryInfo();
            content = IOUtils.toString(inputStream);
            EntityUtils.consume(entity);

            if (content.contains("<?xml") && content.contains("<vcalendar"))
                content = null;
        }

        return content;
    }

    public boolean hasBinaryContent(String contentType) {
        String typeStr = contentType != null ? contentType.toLowerCase() : "";

        return typeStr.contains("image") || typeStr.contains("audio") || typeStr.contains("video") || typeStr.contains("application");
    }

    /*public boolean hasXmlContent(String contentType) {
        String typeStr = contentType != null ? contentType.toLowerCase() : "";

        return typeStr.contains("xml");
    }*/

    public boolean hasPlainTextContent(String contentType) {
        String typeStr = contentType != null ? contentType.toLowerCase() : "";

        return typeStr.contains("text") && !typeStr.contains("html");
    }

}
