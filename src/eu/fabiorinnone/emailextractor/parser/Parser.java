/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.parser;

import eu.fabiorinnone.emailextractor.database.OrientDBAdapter;
import eu.fabiorinnone.emailextractor.utils.Common;
import eu.fabiorinnone.emailextractor.utils.Pair;
import eu.fabiorinnone.emailextractor.utils.Utils;
import eu.fabiorinnone.emailextractor.writer.Writer;
import org.apache.commons.collections4.list.TreeList;
import org.apache.commons.io.IOUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.sax.*;
import org.joda.time.DateTime;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.html.HtmlParser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by fabior on 21/05/16.
 */
public class Parser {

    private OrientDBAdapter dbAdapter;
    private Keywords keywords;
    private String url;
    private int mode;
    private int tNum;

    private Pair<String> foundedPair;

    private static final String REGEX = "[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}";

    public Parser(OrientDBAdapter dbAdapter, Keywords keywords, String url, int mode, int tNum) {
        this.dbAdapter = dbAdapter;
        this.keywords = keywords;
        this.url = url;
        this.mode = mode;
        this.tNum = tNum;

        foundedPair = null;
    }

    private boolean parseKeywords(String content) {
        boolean success = false;
        TreeList<Pair<String>> keywordsList = keywords.getKeywords();
        
        for (Pair<String> pair : keywordsList) {
            if (parseContent(content, pair)) {
                foundedPair = pair;

                String first = foundedPair.getFirst();
                String second = foundedPair.getSecond();

                if (Utils.isVerboseMode(mode)) {
                    System.out.println("INFO: Thread " + tNum + ": Keyword pair (" + first + ";" + second + ") " +
                            "founded in web page at url: " + url);
                }

                success = true;
                break;
            }
        }

        return success;
    }

    public LinkedHashSet<String> parseHtmlPage(String content)
            throws IOException,TikaException,SAXException,NullPointerException {
        if (Utils.isVerboseMode(mode))
            System.out.println("INFO: Thread " + tNum + ": Parsing page at url: " + url);

        LinkContentHandler linkHandler = new LinkContentHandler();
        ContentHandler textHandler = new BodyContentHandler(-1);
        ToHTMLContentHandler toHTMLHandler = new ToHTMLContentHandler();
        TeeContentHandler teeHandler = new TeeContentHandler(linkHandler, textHandler, toHTMLHandler);
        Metadata metadata = new Metadata();
        ParseContext parseContext = new ParseContext();
        HtmlParser parser = new HtmlParser();
        InputStream inputStream = IOUtils.toInputStream(content);

        LinkedHashSet<String> outcomingUrls = new LinkedHashSet<String>();

        if (inputStream != null) {
            parser.parse(inputStream, teeHandler, metadata, parseContext);
            List<Link> links = linkHandler.getLinks();

            String baseUrl = null;

            try {
                baseUrl = Utils.getBaseUrl(url);
            } catch (URISyntaxException e) {
                System.err.println("ERROR: Thread " + tNum + " URISyntaxException on URL: " +
                        url + ": " + e.getMessage());
            }

            for (Link link : links) {
                String linkUri = link.getUri();

                URI uri = null;
                try {
                    uri = new URI(linkUri);

                    String scheme = uri.getScheme();

                    String absUrl;
                    if (Arrays.asList(Common.ignoredSchemas).contains(scheme))
                        absUrl = null;
                    else if (linkUri.startsWith("http"))
                        absUrl = linkUri;
                    else if (linkUri.startsWith("/"))
                        absUrl = baseUrl + linkUri;
                    else if (linkUri.startsWith("./"))
                        absUrl = baseUrl + linkUri.substring(1, linkUri.length());
                    else
                        absUrl = baseUrl + "/" + linkUri;

                    //new URI(absUrl); //workaround for generating exception if absUrl is not valid

                    if (absUrl != null)
                        outcomingUrls.add(absUrl);
                } catch (URISyntaxException e) {
                    System.err.println("ERROR: Thread " + tNum +
                            " URISyntaxException on URI: " + url + ": outcoming URL " + linkUri +
                            " gets an error: " + e.getMessage());
                }
            }

            if (parseKeywords(content)) {
                HashSet<String> emailAddresses = matchEmailAddresses(content);
                for (String emailAddress : emailAddresses) {
                    writeOutput(url, emailAddress, foundedPair);
                    dbAdapter.addEmailToGraph(url, emailAddress, foundedPair, DateTime.now(), tNum);
                }
            }
        }

        return outcomingUrls;
    }

    private HashSet<String> matchEmailAddresses(String content) {
        HashSet<String> emailAddresses = new HashSet<String>();

        Pattern pattern = Pattern.compile(REGEX);
        Matcher matcher = pattern.matcher(content);
        while(matcher.find()) {
            String emailAddress = matcher.group();
            if (!emailAddress.endsWith(".png"))
                emailAddresses.add(emailAddress);
        }

        return emailAddresses;
    }

    public Pair<String> getFoundedPair() {
        return foundedPair;
    }

    private void writeOutput(String url, String attrStr, Pair<String> foundedPair) {
        Writer writer = new Writer(url, attrStr, foundedPair);
        writer.write();
    }

    private boolean parseContent(String content, Pair<String> pair) {
        String firstKeyword = pair.getFirst();
        String secondKeyword = pair.getSecond();
        String firstKeywordCap = firstKeyword.toUpperCase();
        String secondKeywordCap = secondKeyword.toUpperCase();

        if (parseContent(content, firstKeyword, secondKeyword))
            return true;
        else if (parseContent(content, firstKeywordCap, secondKeywordCap))
            return true;

        return false;
    }

    private boolean parseContent(String content, String firstKeyword, String secondKeyword) {
        if (content.contains(firstKeyword) && content.contains(secondKeyword))
            return true;
        else
            return false;
    }
}
