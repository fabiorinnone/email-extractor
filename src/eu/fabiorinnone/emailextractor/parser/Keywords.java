/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.parser;

import eu.fabiorinnone.emailextractor.utils.Pair;
import org.apache.commons.collections4.list.TreeList;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;

/**
 * Created by fabior on 22/05/16.
 */
public class Keywords {

    private TreeList<Pair<String>> keywords;
    private String keywordsFileName;
    private File keywordsFile;

    public Keywords(String keywordsFileName) {
        this.keywordsFileName = keywordsFileName;

        keywords = new TreeList<Pair<String>>();
        keywordsFile = new File(keywordsFileName);

        System.out.println("Loading keywords...");
        try {
            initializeKeywords();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initializeKeywords() throws IOException {
        String currentLine = "";
        LineIterator it = FileUtils.lineIterator(keywordsFile, "UTF-8");
        try {
            while (it.hasNext()) {
                currentLine = it.nextLine();
                String[] split = currentLine.split(";");
                System.out.println(split[0] + ";" + split[1]);
                keywords.add(new Pair<String>(split[0], split[1]));
            }
        }
        finally {
            LineIterator.closeQuietly(it);
        }
    }

    public TreeList<Pair<String>> getKeywords() {
        return keywords;
    }
}
