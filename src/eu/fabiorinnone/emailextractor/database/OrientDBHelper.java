/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.database;

import com.orientechnologies.orient.core.config.OGlobalConfiguration;
import com.orientechnologies.orient.core.exception.OSchemaException;
import com.orientechnologies.orient.core.index.OIndexException;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OProperty;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.tinkerpop.blueprints.impls.orient.*;
import eu.fabiorinnone.emailextractor.utils.Common;
import eu.fabiorinnone.emailextractor.utils.Utils;
import org.apache.commons.collections4.list.TreeList;

import java.io.IOException;
import java.util.*;

/**
 * Created by fabior on 10/05/16.
 */
public class OrientDBHelper {

    private static String urlsClassName = Common.URLS_CLASS_NAME;
    private static String emailsClassName = Common.EMAILS_CLASS_NAME;

    private OrientGraphFactory factory;
    private OrientBaseGraph graph;

    public String dbms;
    public String dbName;
    public String userName;
    public String password;
    public String databasePath;
    public String engine;

    public String database;

    private int mode;

    private TreeList<String> classes;
    private HashMap<String,HashMap<String,OType>> props;
    private HashMap<String,HashMap<List<String>,OClass.INDEX_TYPE>> indexes;

    private HashMap<String,OType> urlsProps;
    private HashMap<List<String>,OClass.INDEX_TYPE> urlsIndexes;
    private HashMap<String,OType> emailsProps;
    private HashMap<List<String>,OClass.INDEX_TYPE> emailsIndexes;

    public OrientDBHelper(HashMap<String,String> properties, int mode) throws IOException {
        super();
        this.mode = mode;
        setProperties(properties);
        initializeFactory();
        initializeSchema();
    }

    private void initializeSchema() {
        graph = factory.getNoTx();

        graph.setUseLightweightEdges(true);

        OGlobalConfiguration.USE_WAL.setValue(false);

        classes = new TreeList<String>();
        classes.add(urlsClassName);
        classes.add(emailsClassName);

        props = new HashMap<String, HashMap<String,OType>>();
        indexes = new HashMap<String,HashMap<List<String>,OClass.INDEX_TYPE>>();

        urlsProps = new HashMap<String,OType>();
        urlsIndexes = new HashMap<List<String>,OClass.INDEX_TYPE>();
        emailsProps = new HashMap<String,OType>();
        emailsIndexes = new HashMap<List<String>,OClass.INDEX_TYPE>();

        initializePropsMaps();
        intializeIndexesMaps();
        createClasses();
    }

    private void initializePropsMaps() {
        urlsProps.put(Common.URLS_PROP_URL, OType.STRING);
        urlsProps.put(Common.URLS_PROP_SEED, OType.BOOLEAN);
        urlsProps.put(Common.URLS_PROP_DEPTH, OType.INTEGER);
        urlsProps.put(Common.URLS_PROP_VISITED, OType.BOOLEAN);
        urlsProps.put(Common.URLS_PROP_DATE_TIME, OType.DATETIME);

        emailsProps.put(Common.EMAILS_PROP_EMAIL, OType.STRING);
        emailsProps.put(Common.EMAILS_PROP_URL, OType.STRING);
        emailsProps.put(Common.EMAILS_PROP_KEY_1, OType.STRING);
        emailsProps.put(Common.EMAILS_PROP_KEY_2, OType.STRING);
        emailsProps.put(Common.EMAILS_PROP_DATE_TIME, OType.DATETIME);

        props.put(urlsClassName, urlsProps);
        props.put(emailsClassName, emailsProps);
    }

    private void intializeIndexesMaps() {
        List<String> urlIndexAttrs = new ArrayList<String>();
        urlIndexAttrs.add(Common.URLS_PROP_URL);
        urlsIndexes.put(urlIndexAttrs, OClass.INDEX_TYPE.UNIQUE_HASH_INDEX);

        List<String> emailIndexAttrs = new ArrayList<String>();
        emailIndexAttrs.add(Common.EMAILS_PROP_EMAIL);
        emailsIndexes.put(emailIndexAttrs, OClass.INDEX_TYPE.NOTUNIQUE_HASH_INDEX);

        indexes.put(urlsClassName, urlsIndexes);
        indexes.put(emailsClassName, emailsIndexes);
    }

    private void createClasses() {
        for (String aClass : classes) {
            createClass(aClass);
            createProperties(aClass, props.get(aClass));
            createIndexes(aClass, indexes.get(aClass));
        }
    }

    private void createClass(String aClass) {
        if (Utils.isVerboseMode(mode))
            System.out.println("INFO: Creating " + aClass + " class on graph");

        try {
            graph.createVertexType(aClass);
        }
        catch (OSchemaException e) {
            if (Utils.isVerboseMode(mode))
                System.out.println("INFO: Class " + aClass + " already exists into graph");
        }
    }

    private void createProperties(String aClass, HashMap<String,OType> props) {
        Set<Map.Entry<String,OType>> entrySet = props.entrySet();
        for (Map.Entry<String,OType> entry : entrySet) {
            String propName = entry.getKey();
            OType propType = entry.getValue();

            addPropertyToClass(aClass, propName, propType);
        }

        graph.commit();
    }

    private void addPropertyToClass(String className, String propName, OType propType) {
        if (Utils.isVerboseMode(mode))
            System.out.println("INFO: Adding property " + propName + " into class " + className);

        OrientVertexType vertexType = graph.getVertexType(className);

        OProperty prop = vertexType.getProperty(propName);
        if (prop == null)
        //try {
            vertexType.createProperty(propName, propType);
        /*}
        catch (OSchemaException e) {*/
        else {
            if (Utils.isVerboseMode(mode))
                System.out.println("INFO: Property " + propName + " already exists into class " + className);
        }
    }

    private void createIndexes(String aClass, HashMap<List<String>,OClass.INDEX_TYPE> indexes) {
        Set<Map.Entry<List<String>,OClass.INDEX_TYPE>> entrySet = indexes.entrySet();
        for (Map.Entry<List<String>,OClass.INDEX_TYPE> entry : entrySet) {
            List<String> propsNames = entry.getKey();
            OClass.INDEX_TYPE indexType = entry.getValue();

            List<String> indexProps = new ArrayList<String>();
            String indexName = "";
            Iterator<String> iterator = propsNames.iterator();
            while(iterator.hasNext()) {
                String propName = iterator.next();
                indexProps.add(propName);
                indexName += propName + "_";
                if (!iterator.hasNext())
                    indexName += "index";
            }

            addIndexToClass(aClass, indexName, indexProps, indexType);
        }

        graph.commit();
    }

    private void addIndexToClass(String className, String indexName,
                                 List<String> indexProps, OClass.INDEX_TYPE indexType) {
        if (Utils.isVerboseMode(mode))
            System.out.println("INFO: Adding index " + indexName + " into class " + className);

        String[] indexPropsArr = indexProps.toArray(new String[indexProps.size()]);

        OrientVertexType vertexType = graph.getVertexType(className);

        try {
            vertexType.createIndex(indexName, indexType, indexPropsArr);
        }
        catch (OIndexException e) {
            if (Utils.isVerboseMode(mode))
                System.out.println("INFO: Index " + indexName + " already exists into class " + className);
        }
    }

    private void initializeFactory() {
        database = engine + ":" + databasePath + "/" + dbName;
        factory = new OrientGraphFactory(database).setupPool(1, 10);
    }

    private void setProperties(HashMap<String,String> properties) throws IOException {
        //this.prop = new Properties();
        //FileInputStream fis = new FileInputStream(fileName);
        //prop.loadFromXML(fis);

        this.dbms = properties.get("orientdb.dbms");
        this.dbName = properties.get("orientdb.database_name");
        this.userName = properties.get("orientdb.user_name");
        this.password = properties.get("orientdb.password");
        this.databasePath = properties.get("orientdb.database_path");
        this.engine = properties.get("orientdb.engine");

        if (Utils.isVerboseMode(mode)) {
            System.out.println("Set the following properties:");
            System.out.println("dbms: " + dbms);
            System.out.println("dbName: " + dbName);
            System.out.println("userName: " + userName);
            System.out.println("databasePath: " + databasePath);
            System.out.println("engine: " + engine);
        }
    }

    public OrientGraphNoTx getNoTxGraph() {
        return factory.getNoTx();
    }

    /*public OrientGraph getTxGraph() {
        return factory.getTx();
    }*/
}
