/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.database;

import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.orientechnologies.orient.core.storage.ORecordDuplicatedException;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.*;
import eu.fabiorinnone.emailextractor.utils.Common;
import eu.fabiorinnone.emailextractor.utils.Pair;
import eu.fabiorinnone.emailextractor.utils.Utils;
import org.jetbrains.annotations.Contract;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * Created by fabior on 07/05/16.
 */
public class OrientDBAdapter {

    //private static OrientDBAdapter instance =  null;

    private OrientBaseGraph graph;
    private int mode;
    private int tNum;

    public OrientDBAdapter(OrientBaseGraph graph, int mode) {
        this(graph, mode, 0);
    }

    public OrientDBAdapter(OrientBaseGraph graph, int mode, int tNum) {
        this.graph = graph;
        this.mode = mode;
        this.tNum = tNum;
    }

    /*public static synchronized OrientDBAdapter getInstance(OrientGraphNoTx graph) {
        if (instance == null) {
            instance = new OrientDBAdapter(graph);
        }
        return instance;
    }*/

    public synchronized void addEmailToGraph(String url, String email, Pair<String> keys, DateTime dateTime, int tNum) {
        if (Utils.isVerboseMode(mode))
            System.out.println("INFO: Thread " + tNum + ": " + dateTime +
                    ": adding founded email " + email + " into database");

        String dateTimeFormatted = Utils.getDateTimeFormatted(dateTime);

        String clusterName = Common.EMAILS_CLASS_NAME;
        OrientVertex emailVertex = graph.addVertex(Common.EMAILS_CLASS_NAME, clusterName);
        emailVertex.setProperty(Common.EMAILS_PROP_URL, url);
        emailVertex.setProperty(Common.EMAILS_PROP_EMAIL, email);
        emailVertex.setProperty(Common.EMAILS_PROP_KEY_1, keys.getFirst());
        emailVertex.setProperty(Common.EMAILS_PROP_KEY_2, keys.getSecond());
        emailVertex.setProperty(Common.EMAILS_PROP_DATE_TIME, dateTimeFormatted);

        Vertex urlVertex = this.getVertexFromUrl(url);
        addEdgeToGraph(urlVertex, emailVertex);
    }

    public synchronized Vertex addUrlToGraph(String urlSource, String urlTarget, DateTime dateTime, int tNum) {
        return addUrlToGraph(urlSource, urlTarget, dateTime, false, tNum);
    }

    public synchronized Vertex addUrlToGraph(String urlSource, String urlTarget,
                                             DateTime dateTime, boolean isSeed, int tNum) {
        String seed = "";
        if (isSeed)
            seed = " seed";

        String dateTimeFormatted = Utils.getDateTimeFormatted(dateTime);

        if (Utils.isVerboseMode(mode))
            System.out.println("INFO: Thread " + tNum + ": " + dateTimeFormatted +
                    ": adding url " + urlTarget + seed + " into graph");

        Integer sourceDepth = null;
        Integer targetDepth = null;
        if (isSeed)
            targetDepth = 1;
        else {
            sourceDepth = this.getVertexDepth(urlSource);
            targetDepth = sourceDepth + 1;
        }

        Vertex sourceVertex = null;
        Vertex targetVertex = null;
        try {
            String clusterName = Common.URLS_CLASS_NAME;
            if (!vertexExists(urlTarget) && urlTarget != null) {
                targetVertex = graph.addVertex(Common.URLS_CLASS_NAME, clusterName);
                targetVertex.setProperty(Common.URLS_PROP_URL, urlTarget);
                targetVertex.setProperty(Common.URLS_PROP_SEED, isSeed);
                targetVertex.setProperty(Common.URLS_PROP_DEPTH, targetDepth);
                targetVertex.setProperty(Common.URLS_PROP_VISITED, false);
                targetVertex.setProperty(Common.URLS_PROP_DATE_TIME, dateTimeFormatted);

                if (urlSource != null) {
                    sourceVertex = getVertexFromUrl(urlSource);
                    addEdgeToGraph(sourceVertex, targetVertex);
                }
            } else {
                if (Utils.isVerboseMode(mode))
                    System.out.println("INFO: Thread + " + tNum + ": Vertex with url " + urlTarget + " already exists");

                targetDepth = this.getVertexDepth(urlTarget);
                this.setVertexDepth(urlTarget, Math.min(targetDepth, sourceDepth + 1));
            }
        } catch (ORecordDuplicatedException e) {
            if (Utils.isVerboseMode(mode))
                System.out.println("INFO: Thread " + tNum + ": An error occurred while loading " + urlTarget);
            if (Utils.isDebugMode(mode))
                System.err.println("DEBUG: Thread " + tNum + ": " + e.getClass().toString() + " " + e.getMessage());
            Vertex vertex = this.getVertexFromUrl(urlTarget);
            if (vertex != null) {
                targetDepth = this.getVertexDepth(vertex);
                if (targetDepth != null && sourceDepth != null) {
                    this.setVertexDepth(vertex, Math.min(targetDepth, sourceDepth + 1));
                    if (Utils.isDebugMode(mode)) {
                        String minDepth = "MIN(" + targetDepth + "," + (sourceDepth + 1) + ")";
                        System.out.println("DEBUG: Thread " + tNum + ": Updating depth of vertex with " + minDepth);
                    }
                }
            }
            graph.removeVertex(targetVertex);
        } catch (NullPointerException e) {
            if (Utils.isDebugMode(mode))
                System.err.println("DEBUG: Thread " + tNum + ": OrientDBAdapter error " +
                        e.getClass().toString() + " " + e.getMessage());
            //if (targetVertex != null)
                //graph.removeVertex(targetVertex);
        }

        return targetVertex;
    }

    @Contract("null, _ -> null; !null, null -> null")
    private synchronized Edge addEdgeToGraph(Vertex source, Vertex target) {
        if (source != null && target != null)
            return graph.addEdge(null, source, target, "E");
        else
            return null;
    }

    public void executeQuery(String query) {
        graph.command(new OCommandSQL(query)).execute();
    }

    public synchronized Iterable<Vertex> getSeedVertices() {
        String query = "SELECT FROM " + Common.URLS_CLASS_NAME + " WHERE " + Common.URLS_PROP_SEED + " = true";

        Iterable<Vertex> seeds = graph.command(new OCommandSQL(query)).execute();

        return seeds;
    }

    public synchronized LinkedHashSet<String> getSeedUrls() {
        LinkedHashSet<String> seedUrls = new LinkedHashSet<String>();

        Iterable<Vertex> vertices = this.getSeedVertices();
        for (Vertex vertex : vertices) {
            String url = vertex.getProperty(Common.URLS_PROP_URL);
            seedUrls.add(url);
        }

        return seedUrls;
    }

    public synchronized void visitUrl(String url) {
        url = Utils.escapeUrl(url);
        String query = "UPDATE " + Common.URLS_CLASS_NAME + " SET " + Common.URLS_PROP_VISITED +
                " = true WHERE " + Common.URLS_PROP_URL + " = '" + url + "'";
        graph.command(new OCommandSQL(query)).execute();
    }

    public synchronized boolean isVisited(String url) {
        return isVisited(getVertexFromUrl(url));
    }

    public synchronized boolean isVisited(Vertex vertex) {
        if (vertex.getProperty(Common.URLS_PROP_VISITED) == "true")
            return true;
        else
            return false;
    }

    public synchronized LinkedHashSet<String> getNotVisitedUrls(int num) {
        LinkedHashSet<String> urls = new LinkedHashSet<String>();
        String query = "SELECT FROM " + Common.URLS_CLASS_NAME +
                " WHERE " + Common.URLS_PROP_VISITED + " = false limit " +  num;

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();

        for (Vertex vertex : vertices) {
            String url = vertex.getProperty(Common.URLS_PROP_URL);
            urls.add(url);
        }

        return urls;
    }

    private synchronized Integer getVertexDepth(String url) {
        url = Utils.escapeUrl(url);
        String query = "SELECT " + Common.URLS_PROP_DEPTH + " FROM " + Common.URLS_CLASS_NAME +
                " WHERE " + Common.URLS_PROP_URL + " = '" + url + "'";

        Integer depth = null;

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        Vertex vertex = null;
        if (vertices.iterator().hasNext()) {
            vertex = vertices.iterator().next();
            depth = vertex.getProperty(Common.URLS_PROP_DEPTH);
        }

        return depth;
    }

    private synchronized Integer getVertexDepth(Vertex vertex) {
        return vertex.getProperty(Common.URLS_PROP_DEPTH);
    }

    private synchronized void setVertexDepth(String url, Integer depth) {
        url = Utils.escapeUrl(url);
        String query = "UPDATE " + Common.URLS_CLASS_NAME + " SET " + Common.URLS_PROP_DEPTH +
                " = " + depth + " WHERE " + Common.URLS_PROP_URL + " = '" + url + "'";
        graph.command(new OCommandSQL(query)).execute();
    }

    private synchronized void setVertexDepth(Vertex vertex, Integer depth) {
        vertex.setProperty(Common.URLS_PROP_DEPTH, depth);
    }

    public synchronized void removeVertex(Vertex vertex) {
        graph.removeVertex(vertex);
    }

    public synchronized Vertex getVertexFromUrl(String url) {
        url = Utils.escapeUrl(url);
        String query = "SELECT FROM " + Common.URLS_CLASS_NAME + " WHERE " + Common.URLS_PROP_URL + " = '" + url + "'";
        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();

        Iterator<Vertex> verticesIterator = vertices.iterator();
        if (verticesIterator.hasNext())
            return verticesIterator.next();
        else
            return null;
    }

    public synchronized boolean vertexExists(String url) {
        url = Utils.escapeUrl(url);
        String query = "SELECT FROM " + Common.URLS_CLASS_NAME + " WHERE " + Common.URLS_PROP_URL + " = '" + url + "'";
        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();

        if (vertices.iterator().hasNext())
            return true;
        else
            return false;
    }

    public synchronized long getVertexCount() {
        String query = "SELECT COUNT(*) AS total FROM " + Common.URLS_CLASS_NAME;

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        Vertex vertex = vertices.iterator().next();
        long count = vertex.getProperty("total");
        return count;
    }

    public synchronized boolean areThereVertices() {
        String query = "SELECT " + Common.URLS_PROP_URL + " FROM " + Common.URLS_CLASS_NAME + " LIMIT 100";

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        if (vertices.iterator().hasNext())
            return true;
        else
            return false;
    }

    public synchronized boolean areThereNotVisitedVertices() {
        String query = "SELECT " + Common.URLS_PROP_URL +
                " FROM " + Common.URLS_CLASS_NAME +
                " WHERE " + Common.URLS_PROP_VISITED + " = FALSE";

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        if (vertices.iterator().hasNext())
            return true;
        else
            return false;
    }

    public synchronized long getUrlsCount() {
        String query = "SELECT COUNT(*) AS total FROM " + Common.URLS_CLASS_NAME;

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        Vertex vertex = vertices.iterator().next();
        long count = vertex.getProperty("total");
        return count;
    }

    public synchronized long getEmailCount() {
        String query = "SELECT COUNT(*) AS total FROM " + Common.EMAILS_CLASS_NAME;

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        Vertex vertex = vertices.iterator().next();
        long count = vertex.getProperty("total");
        return count;
    }

    public synchronized long getEmailCountUntilDateTime(DateTime dateTime) {
        String query = "SELECT FROM " + Common.EMAILS_CLASS_NAME;

        long count = 0;
        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();
        for (Vertex vertex : vertices) {
            Date date = vertex.getProperty(Common.EMAILS_PROP_DATE_TIME);
            DateTime dateTimeVertex = new DateTime(date);
            if (dateTimeVertex.compareTo(dateTime) < 0)
                count++;
        }

        return count;
    }

    public synchronized Integer diameter() {
        Integer max = null;
        String query = "SELECT MAX(" + Common.URLS_PROP_DEPTH + ") AS max_depth FROM " + Common.URLS_CLASS_NAME;

        Iterable<Vertex> vertices = graph.command(new OCommandSQL(query)).execute();

        Vertex vertex = null;
        if (vertices.iterator().hasNext()) {
            vertex = vertices.iterator().next();
            max = vertex.getProperty("max_depth");
        }

        return max - 1;
    }
}
