/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.crawler;

import com.orientechnologies.orient.core.exception.ORecordNotFoundException;
import com.orientechnologies.orient.core.sql.OCommandSQLParsingException;
import com.orientechnologies.orient.core.sql.parser.TokenMgrError;
import com.tinkerpop.blueprints.Vertex;
import eu.fabiorinnone.emailextractor.database.OrientDBAdapter;
import eu.fabiorinnone.emailextractor.fetcher.PageFetcher;
import eu.fabiorinnone.emailextractor.parser.Keywords;
import eu.fabiorinnone.emailextractor.parser.Parser;
import eu.fabiorinnone.emailextractor.utils.Common;
import eu.fabiorinnone.emailextractor.utils.Utils;
import org.apache.tika.exception.TikaException;
import org.joda.time.DateTime;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by fabior on 10/05/16.
 */
public class Crawler {

    private OrientDBAdapter dbAdapter;
    private Keywords keywords;
    private DateTime endTime;
    private int tNum;
    private int mode;

    private LinkedHashSet<String> urlsSet;
    private PageFetcher fetcher;

    public Crawler(OrientDBAdapter dbAdapter, Keywords keywords, DateTime endTime,
                   LinkedHashSet<String> urlSources, int tNum, int mode) {
        this.dbAdapter = dbAdapter;
        this.keywords = keywords;
        this.endTime = endTime;
        this.tNum = tNum;
        this.mode = mode;

        urlsSet = urlSources;
    }

    public void crawl() {
        while(urlsSet.size() > 0 || dbAdapter.areThereNotVisitedVertices()) {
            LinkedHashSet<String> tmpSet = new LinkedHashSet<String>(urlsSet);
            if (urlsSet.size() > 0) {
                for (String url : tmpSet) {
                    //if (Utils.isDebugMode(mode))
                        //Utils.printHeapMemoryInfo();

                    if (Utils.isDebugMode(mode))
                        System.out.println("DEBUG: Thread " + tNum + " queue size: " + urlsSet.size());

                    try {
                        crawl(url);
                    } catch (Exception e) {
                        if (Utils.isDebugMode(mode)) {
                            System.err.println("DEBUG: Thread " + tNum + " crawler error");
                            //e.printStackTrace();
                            if (urlsSet.size() == 0) {
                                reInitializeQueue(Common.START_QUEUE_SIZE);
                            }

                            dbAdapter.visitUrl(url);
                        }
                    }

                    try {
                        urlsSet.remove(url);
                    } catch (NoSuchElementException e) {
                        System.err.println("DEBUG: Thread " + tNum + " NoSuchElementException " + e.getMessage());
                        //if (urlsSet.size() == 0)
                            reInitializeQueue(Common.START_QUEUE_SIZE);
                    }

                    if (urlsSet.size() > Common.MAX_SIZE || urlsSet.size() == 0) {
                        if (urlsSet.size() > Common.MAX_SIZE) {
                            if (Utils.isDebugMode(mode))
                                System.out.println("DEBUG: Thread " + tNum + " flushing queue...");
                        } else {
                            if (Utils.isDebugMode(mode))
                                System.out.println("DEBUG: Thread " + tNum + " populating queue from database...");
                        }
                        reInitializeQueue(Common.START_QUEUE_SIZE);
                    }
                }
            }
            else {
                if (Utils.isDebugMode(mode))
                    System.out.println("DEBUG: Thread " + tNum + " populating queue from database...");

                reInitializeQueue(Common.START_QUEUE_SIZE);
            }
        }

        System.out.println("INFO: No more URLs to visit in current thread...");
    }

    private void reInitializeQueue(int num) {
        urlsSet = new LinkedHashSet<String>();
        urlsSet = dbAdapter.getNotVisitedUrls(num);
    }

    private void crawl(String sourceUrl) throws Exception {
        fetcher = new PageFetcher(sourceUrl, mode, tNum);

        LinkedHashSet<String> targetUrls = null;

        try {
            String content = null;
            //try {
            content = fetcher.fetchPage();
            //} catch (NullPointerException e) {
            //System.err.println("An error occurred");
            //}

            if (content != null) {
                Parser parser = new Parser(dbAdapter, keywords, sourceUrl, mode, tNum);
                targetUrls = parser.parseHtmlPage(content);

                if (targetUrls != null && targetUrls.size() > 0) {
                    for (String targetUrl : targetUrls) {
                        if (DateTime.now().compareTo(endTime) < 0) {
                            targetUrl = Utils.removeFragmentFromUrl(targetUrl);
                            Vertex vertex = null;
                            try {
                                if (targetUrl != null)
                                    vertex = dbAdapter.getVertexFromUrl(targetUrl);
                            } catch (OCommandSQLParsingException e) {
                                System.err.println("ERROR: Thread " + tNum + ": An error occurred on querying database");
                                //e.printStackTrace();
                                //break;
                            }
                            if (vertex == null) {
                                addUrlToGraph(sourceUrl, targetUrl);
                            }
                            else {
                                String vertexUrl = vertex.getProperty(Common.URLS_PROP_URL);
                                if (vertexUrl != null) {
                                    if (!vertexUrl.equals(targetUrl)) {
                                        addUrlToGraph(sourceUrl, targetUrl);
                                    }
                                }
                            }
                        }
                        else {
                            if (Utils.isVerboseMode(mode))
                                System.out.println("INFO: Suspending thread " + tNum);

                            while (true); //workaround for suspending thread before calculating graph diameter
                        }
                    }
                }
                //dbAdapter.visitUrl(sourceUrl);
            }
            else
                System.out.println("INFO: Thread " + tNum + ": URL " + sourceUrl +
                        " contains not valid content");
            //dbAdapter.visitUrl(sourceUrl);
        } catch (URISyntaxException e) {
            System.err.println("ERROR: Thread " + tNum + ": URL " + sourceUrl + " is not valid");
        } catch (IOException e) {
            System.err.println("ERROR: Thread " + tNum + ": URL " + sourceUrl + ": " + e.getMessage());
        } catch (SAXException e) {
            System.err.println("ERROR: Thread " + tNum + ": URL " + sourceUrl + ": " + e.getMessage());
        } catch (TikaException e) {
            System.err.println("ERROR: Thread " + tNum + ": URL " + sourceUrl + ": " + e.getMessage());
        } catch (TokenMgrError e) {
            System.err.println("ERROR: Thread " + tNum + ": URL " + sourceUrl + ": " + e.getMessage());
        } finally {
            if (urlsSet.size() == 0) {
                reInitializeQueue(Common.START_QUEUE_SIZE);
            }
        }

        try {
            dbAdapter.visitUrl(sourceUrl);
        }
        catch (Exception e) {
            if (sourceUrl != null)
                dbAdapter.removeVertex(dbAdapter.getVertexFromUrl(sourceUrl));

            if (urlsSet.size() == 0) {
                reInitializeQueue(Common.START_QUEUE_SIZE);
            }
        }
    }

    private void addUrlToGraph(String sourceUrl, String targetUrl) {
        DateTime dateTime = DateTime.now();
        Vertex vertex = dbAdapter.addUrlToGraph(sourceUrl, targetUrl, dateTime, tNum);
        if (vertex != null) {
            try {
                if (vertex.getProperty(Common.URLS_PROP_URL) == null)
                    dbAdapter.removeVertex(vertex);
            } catch (ORecordNotFoundException e) {
                System.err.println("ERROR: Thread " + tNum + " ORecordNotFoundException: " + e.getMessage());
            }
        }
        urlsSet.add(targetUrl);
    }
}
