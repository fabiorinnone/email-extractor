/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.main;

import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph;
import eu.fabiorinnone.emailextractor.database.OrientDBAdapter;
import eu.fabiorinnone.emailextractor.database.OrientDBHelper;
import eu.fabiorinnone.emailextractor.graph.ExpandGraph;
import eu.fabiorinnone.emailextractor.parser.Keywords;
import eu.fabiorinnone.emailextractor.utils.Common;
import eu.fabiorinnone.emailextractor.graph.SeedsLoader;
import com.tinkerpop.blueprints.Vertex;
import eu.fabiorinnone.emailextractor.utils.PrintMessages;
import eu.fabiorinnone.emailextractor.utils.Utils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.*;

public class Main {

    private static final String BUNDLE = "eu.fabiorinnone.emailextractor.main.resources.database";

    private static ResourceBundle bundle = null;

    public static int EXIT_SUCCESS = 0;
    public static int EXIT_ARGS_ERROR = 1;
    public static int EXIT_DIAM_ERROR = 2;

    public static String USAGE_MESSAGE =
            "Usage: java -jar email-extractor.jar [MODE] OPTION [-diameter] seeds_file keywords_file";

    public static String HELP_MESSAGE =
            "\nMODE:\t-[verbose|debug]\n\n" +
            "OPTION:\t-[s|m|d|h|w]\n\t-s seconds\n\t-m minutes\n\t-h hours\t\n\t-d days\n\t-w weeks";

    public static void main(String[] args) {

        if (args.length < 4 || args.length > 6)
            exitWithArgsError();

        String[] modeArgs = {"verbose", "debug"};
        String[] optionArgs = {"s", "m", "h", "d", "w"};
        String modeVal = null;
        String optionArg = null;
        String optionVal = null;
        String urlSeedsFileName = null;
        String keyWordsFileName = null;
        boolean diameter = false;

        int endTimeNum = 0;

        int mode = Common.INFO_MODE;

        if (args.length == 4) {
            optionArg = args[0].substring(1);
            optionVal = args[1];
            urlSeedsFileName = args[2];
            keyWordsFileName = args[3];
        }
        else if (args.length == 5) {
            if (args[0].charAt(0) != '-')
                exitWithArgsError();

            boolean modeSet = false;
            for (String arg : modeArgs) {
                if (args[0].substring(1).equals(arg)) {
                    modeSet = true;
                    break;
                }
            }

            if (modeSet) {
                if (args[1].charAt(0) != '-')
                    exitWithArgsError();

                modeVal = args[0].substring(1);
                optionArg = args[1].substring(1);
                optionVal = args[2];
                urlSeedsFileName = args[3];
                keyWordsFileName = args[4];
            }
            else {
                if (args[0].charAt(0) != '-')
                    exitWithArgsError();
                if (args[2].charAt(0) != '-')
                    exitWithArgsError();

                optionArg = args[0].substring(1);
                optionVal = args[1];
                String diameterVal = args[2].substring(1);
                urlSeedsFileName = args[3];
                keyWordsFileName = args[4];

                if (diameterVal.equals("diameter"))
                    diameter = true;
            }
        }
        else {
            if (args[0].charAt(0) != '-')
                exitWithArgsError();
            if (args[1].charAt(0) != '-')
                exitWithArgsError();
            if (args[3].charAt(0) != '-')
                exitWithArgsError();

            modeVal = args[0].substring(1);
            optionArg = args[1].substring(1);
            optionVal = args[2];
            String diameterVal = args[3].substring(1);
            urlSeedsFileName = args[4];
            keyWordsFileName = args[5];

            if (diameterVal.equals("diameter"))
                diameter = true;
        }

        if (optionArg.length() != 1)
            exitWithArgsError();
        if (!Arrays.asList(optionArgs).contains(optionArg))
            exitWithArgsError();
        if (NumberUtils.isNumber(optionVal))
            endTimeNum = Integer.parseInt(optionVal);
        else
            exitWithArgsError();

        if (modeVal != null) {
            if (!modeVal.equals("verbose") && !modeVal.equals("debug"))
                exitWithArgsError();

            if (modeVal.equals("verbose"))
                mode = Common.VERBOSE_MODE;
            else
                mode = Common.DEBUG_MODE;
        }

        printInitialMessage();

        HashMap<String,String> orientdbProperties = new HashMap<String,String>();

        orientdbProperties.put("orientdb.dbms", getString("orientdb.dbms"));
        orientdbProperties.put("orientdb.database_name", getString("orientdb.database_name"));
        orientdbProperties.put("orientdb.user_name", getString("orientdb.user_name"));
        orientdbProperties.put("orientdb.password", getString("orientdb.password"));
        orientdbProperties.put("orientdb.database_path", getString("orientdb.database_path"));
        orientdbProperties.put("orientdb.engine", getString("orientdb.engine"));

        System.out.println("INFO: Estabilishing connection to OrientDB...");

        OrientDBHelper orientDBHelper = null;
        OrientBaseGraph graph = null;
        try {
            orientDBHelper = new OrientDBHelper(orientdbProperties, mode);
        } catch (IOException e) {
            System.err.println("ERROR: Problem reading properties file");
            e.printStackTrace();
        }

        graph = orientDBHelper.getNoTxGraph();
        //graph.getRawGraph().declareIntent(new OIntentMassiveInsert());
        OrientDBAdapter orientDBAdapter = new OrientDBAdapter(graph, mode);

        Keywords keywords = new Keywords(keyWordsFileName);

        System.out.println("INFO: Checking if there are vertices not visited on graph");
        boolean areThereVertices = orientDBAdapter.areThereVertices();

        int seedsNum = 0;

        if (!areThereVertices) {
            System.out.println("INFO: There are not vertices not visited on graph");
            SeedsLoader seedsLoader = new SeedsLoader(orientDBAdapter, urlSeedsFileName);
            try {
                seedsNum = seedsLoader.insertSeedsIntoDatabase();
            } catch (IOException e) {
                System.err.println("ERROR: Problem reading url seeds file");
                e.printStackTrace();
            }
        }
        else {
            System.out.println("INFO: There are vertices not visited on graph");

        }

        String timeMessage = "INFO: Executing bot until ";

        /*
         * START TIME
         */
        DateTime startTime = DateTime.now();
        System.out.println("INFO: Start time: " + Utils.getDateTimeFormatted(startTime));

        /*
         * END TIME
         */
        DateTime endTime = null;
        if (optionArg.equals("s")) {
            endTime = startTime.plusSeconds(endTimeNum);
            timeMessage += endTimeNum + " seconds";
        }
        else if (optionArg.equals("m")) {
            endTime = startTime.plusMinutes(endTimeNum);
            timeMessage += endTimeNum + " minutes";
        }
        else if (optionArg.equals("h")) {
            endTime = startTime.plusHours(endTimeNum);
            timeMessage += endTimeNum + " hours";
        }
        else if (optionArg.equals("d")) {
            endTime = startTime.plusDays(endTimeNum);
            timeMessage += endTimeNum + " days";
        }
        else if (optionArg.equals("w")) {
            endTime = startTime.plusWeeks(endTimeNum);
            timeMessage += endTimeNum + " weeks";
        }
        System.out.println(timeMessage);

        if (!areThereVertices) {
            Iterable<Vertex> seedsVertices = orientDBAdapter.getSeedVertices();
            Iterator<Vertex> iterator = seedsVertices.iterator();
            int threadNum = 1;
            while (iterator.hasNext()) {
                LinkedHashSet<String> urls = new LinkedHashSet<String>();
                for (int i = 0; i < Common.URLS_FOR_THREAD; i++) {
                    if (iterator.hasNext()) {
                        Vertex vertex = iterator.next();
                        String url = vertex.getProperty(Common.URLS_PROP_URL);
                        urls.add(url);
                    } else break;
                }
                OrientDBAdapter orientDBAdapter1 = new OrientDBAdapter(orientDBHelper.getNoTxGraph(), mode, threadNum);
                Thread expandGraph = new ExpandGraph(orientDBAdapter1, keywords, urls, threadNum++, mode, endTime);
                expandGraph.start();
            }
        }
        else {
            int threadNum = 1;
            int threads = Common.THREAD_NUM;
            while(threadNum <= threads) {
                OrientDBAdapter orientDBAdapter1 = new OrientDBAdapter(orientDBHelper.getNoTxGraph(), mode, threadNum);
                if (Utils.isDebugMode(mode))
                    System.out.println("DEBUG: Populating queue with first 100 not visited URLs");
                LinkedHashSet<String> tmpUrls = orientDBAdapter1.getNotVisitedUrls(threadNum * Common.URLS_FOR_THREAD);
                LinkedHashSet<String> urls = new LinkedHashSet<String>();
                int count = 1;
                for (String tmpUrl : tmpUrls) {
                    if (count > tmpUrls.size() - Common.URLS_FOR_THREAD)
                        urls.add(tmpUrl);
                    count++;
                }
                Thread expandGraph = new ExpandGraph(orientDBAdapter1, keywords, urls, threadNum++, mode, endTime);
                expandGraph.start();
            }
        }

        Timer timer = new Timer();
        timer.schedule(new PrintMessages(orientDBAdapter, mode), Common.INTERVAL, Common.INTERVAL);

        while(DateTime.now().compareTo(endTime) < 0);

        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("INFO: End time: " + Utils.getDateTimeFormatted(DateTime.now()));

        System.out.println("INFO: Total email addresses extracted: " +  orientDBAdapter.getEmailCount());

        DateTime oneHourLater = startTime.plusHours(1);
        DateTime oneDayLater = startTime.plusDays(1);
        DateTime oneWeekLater = startTime.plusWeeks(1);
        DateTime oneMonthLater = startTime.plusMonths(1);

        System.out.println("INFO: Total email addresses extracted until one hour: " +
                orientDBAdapter.getEmailCountUntilDateTime(oneHourLater));
        System.out.println("INFO: Total email addresses extracted until one day: " +
                orientDBAdapter.getEmailCountUntilDateTime(oneDayLater));
        System.out.println("INFO: Total email addresses extracted until one week: " +
                orientDBAdapter.getEmailCountUntilDateTime(oneWeekLater));
        System.out.println("INFO: Total email addresses extracted until one month: " +
                orientDBAdapter.getEmailCountUntilDateTime(oneMonthLater));

        graph.commit();

        if (diameter) {
            try {
                long tStart = System.currentTimeMillis();
                if (Utils.isVerboseMode(mode))
                    System.out.println("INFO:: Calculating graph diameter...");
                //String diam = String.valueOf(GraphUtils.diameter(graph));
                String diam = String.valueOf(orientDBAdapter.diameter());
                System.out.println("INFO: Diameter of graph is: " + diam);
                long tEnd = System.currentTimeMillis();
                long tElapsed = tEnd - tStart;

                if (Utils.isVerboseMode(mode))
                    System.out.println("INFO: Time elapsed for calculating diameter (ms): " + tElapsed);

                System.out.println("INFO: Exiting");
            } catch(Exception e) {
                graph.shutdown();
                System.exit(EXIT_DIAM_ERROR);
            }
        }

        graph.shutdown();
        System.exit(EXIT_SUCCESS);
    }

    private static void exitWithArgsError() {
        System.out.println(USAGE_MESSAGE);
        System.out.println(HELP_MESSAGE);
        System.exit(EXIT_ARGS_ERROR);
    }

    private static void printInitialMessage() {
        System.out.println("EmailExtractor Copyright (C) Fabio Rinnone");
        //System.out.println("This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.");
        //System.out.println("This is free software, and you are welcome to redistribute it");
        //System.out.println("under certain conditions; type `show c' for details.");
    }

    /*private static void deleteOutFileIfExists() {
        File file = new File("out.txt");
        if (file.exists())
            file.delete();
    }*/

    public static ResourceBundle getResourceBundle() {
        if(bundle == null) {
            bundle = ResourceBundle.getBundle(BUNDLE);
        }
        return bundle;
    }

    public static String getString(String key) {
        String value = null;
        try {
            value = getResourceBundle().getString(key);
        }
        catch(MissingResourceException e) {
            System.out.println("java.util.MissingResourceException: Couldn't find value for: " + key);
        }
        if(value == null) {
            value = "Could not find resource: " + key;
        }
        return value;
    }
}
