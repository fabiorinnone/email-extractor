/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.graph;

import eu.fabiorinnone.emailextractor.database.OrientDBAdapter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;

/**
 * Created by fabior on 09/05/16.
 */
public class SeedsLoader {

    private OrientDBAdapter dbAdapter;
    private String urlSeedsFileName;
    private int tNum;

    private File urlSeedsFile;

    public SeedsLoader(OrientDBAdapter dbAdapter, String urlSeedsFileName) {
        this(dbAdapter, urlSeedsFileName, 0);
    }

    public SeedsLoader(OrientDBAdapter dbAdapter, String urlSeedsFileName, int tNum) {
        this.dbAdapter = dbAdapter;
        this.urlSeedsFileName = urlSeedsFileName;
        this.tNum = tNum;

        urlSeedsFile = new File(urlSeedsFileName);
    }

    public int insertSeedsIntoDatabase() throws IOException {
        String currentLine = "";
        int lineNumber = 0;
        LineIterator it = FileUtils.lineIterator(urlSeedsFile, "UTF-8");
        try {
            while (it.hasNext()) {
                currentLine = it.nextLine();
                lineNumber++;
                dbAdapter.addUrlToGraph(null, currentLine, DateTime.now(), true, tNum);
            }
        }
        finally {
            LineIterator.closeQuietly(it);
        }

        return lineNumber;
    }
}
