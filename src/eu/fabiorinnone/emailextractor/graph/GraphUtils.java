/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.graph;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.oupls.jung.GraphJung;
import edu.uci.ics.jung.algorithms.shortestpath.UnweightedShortestPath;

import java.util.Collections;
import java.util.HashSet;

/**
 * Created by fabior on 22/05/16.
 */
public class GraphUtils {

    @Deprecated
    public static Long diameter(Graph orientGraph) {
        GraphJung<Graph> graph = new GraphJung<Graph>(orientGraph);
        Long diameter = 0l;
        UnweightedShortestPath<Vertex, Edge> metrics = new UnweightedShortestPath<Vertex, Edge>(graph);
        HashSet<Long> distances = new HashSet<Long>();
        Iterable<Vertex> vertices = graph.getVertices();
        for (Vertex source : vertices) {
            for (Vertex target : vertices) {
                if (source != target) {
                    try {
                        distances.add(metrics.getDistance(source, target).longValue());
                    } catch (NullPointerException e) {
                        distances.add(0l);
                    }
                }
            }
            System.out.print(".");
        }
        System.out.println();
        //Double diameter = DistanceStatistics.diameter(graph, metrics);
        diameter = Collections.max(distances);
        return diameter;
    }
}
