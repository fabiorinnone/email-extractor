/*
 * EmailExtractor: a bot able to obtain email addresses from web.
 * Copyright (C) 2016 Fabio Rinnone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package eu.fabiorinnone.emailextractor.graph;

import eu.fabiorinnone.emailextractor.crawler.Crawler;
import eu.fabiorinnone.emailextractor.database.OrientDBAdapter;
import eu.fabiorinnone.emailextractor.parser.Keywords;
import eu.fabiorinnone.emailextractor.utils.Utils;
import org.joda.time.DateTime;

import java.util.LinkedHashSet;

/**
 * Created by fabior on 23/05/16.
 */
public class ExpandGraph extends Thread {

    private Thread thread;

    private OrientDBAdapter dbAdapter;
    private Keywords keywords;
    private LinkedHashSet<String> urlSources;
    private int tNum;
    private int mode;
    private DateTime endTime;

    public ExpandGraph(OrientDBAdapter dbAdapter, Keywords keywords,
                       LinkedHashSet<String> urlSources, int tNum, int mode, DateTime endTime) {
        this.dbAdapter = dbAdapter;
        this.keywords = keywords;
        this.urlSources = urlSources;
        this.tNum = tNum;
        this.mode = mode;
        this.endTime = endTime;
    }

    @Override
    public void run() {
        System.out.println("INFO: Thread " + tNum + ": starting crawler");
        Crawler crawler = new Crawler(dbAdapter, keywords, endTime, urlSources, tNum, mode);
        crawler.crawl();
    }

    @Override
    public void start() {
        if (Utils.isVerboseMode(mode)) {
            System.out.println("INFO: Starting thread number " + tNum);
            System.out.println("INFO: Source URLs:");
            for (String urlSource : urlSources) {
                System.out.println(urlSource);
            }
        }
        if (thread == null) {
            thread = new Thread (this); //TODO check this
            thread.start();
        }
    }
}
